<?php
add_action("admin_init", function () {
    if (!empty($_REQUEST['action'])) :
        if ($_REQUEST['action'] == 'export_csv') :
            global $wpdb;
            $table_name = $wpdb->prefix . 'etado_mail_data'; // do not forget about tables prefix
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (!empty($ids)):
                $data_db = $wpdb->get_results("SELECT * FROM $table_name WHERE id IN(" . implode(",", $ids) . ")", ARRAY_A);
                foreach ($data_db as $key => $value) :
                    $data_db[$key]['user_created'] = get_the_author_meta('display_name', $value['user_created']);
                    $data_db[$key]['created_at'] = date('d/m/Y', strtotime($value['created_at']));
                endforeach;

                header('Content-type: text/csv');
                header('Content-Disposition: attachment; filename=Export_etado_mail_' . date('m-d-Y_hia') . '".csv"');
                header('Pragma: no-cache');
                header('Expires: 0');

                $file = fopen('php://output', 'w');

                fputcsv($file, array_keys($data_db[0]));

                foreach ($data_db as $value) {
                    fputcsv($file, $value);
                }
                exit();

            else:
                add_action('admin_notices', function () {
                    printf('<div class="error">%s</div>', 'Chọn ID trước khi tải');
                });
            endif;
        endif;
    endif;
});
