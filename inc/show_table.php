<?php
if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}
require_once ('export-csv.php');
require_once ('rest-api.php');
/**
 * Data_Email_Table_Example_List_Table class that will display our custom table
 * records in nice table
 */
class Data_Email_Table_Example_List_Table extends WP_List_Table
{
    /**
     * [REQUIRED] You must declare constructor and give some basic params
     */
    function __construct()
    {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'person',
            'plural' => 'persons',
        ));
    }

    /**
     * [REQUIRED] this is a default column renderer
     *
     * @param $item - row (key, value array)
     * @param $column_name - string (key)
     * @return HTML
     */
    function column_default($item, $column_name)
    {

        return $item[$column_name];
    }

    /**
     * [OPTIONAL] this is example, how to render specific column
     *
     * method name must be like this: "column_[column_name]"
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_age($item)
    {
        return '<em>' . $item['age'] . '</em>';
    }

    /**
     * [OPTIONAL] this is example, how to render column with actions,
     * when you hover row "Edit | Delete" links showed
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_id($item)
    {
        // links going to /admin.php?page=[your_plugin_page][&other_params]
        // notice how we used $_REQUEST['page'], so action will be done on curren page
        // also notice how we use $this->_args['singular'] so in this example it will
        // be something like &person=2
        $actions = array(
            'edit' => sprintf('<a href="?page=list_mail_form&id=%s">%s</a>', $item['id'], __('Edit', 'maildata')),
            'delete' => sprintf('<a href="?page=%s&action=delete&id=%s">%s</a>', $_REQUEST['page'], $item['id'], __('Delete', 'maildata')),
        );

        return sprintf('%s %s', $item['id'], $this->row_actions($actions));
    }

    /**
     * [REQUIRED] this is how checkbox column renders
     *
     * @param $item - row (key, value array)
     * @return HTML
     */
    function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="id[]" value="%s" />',
            $item['id']
        );
    }

    /**
     * [REQUIRED] This method return columns to display in table
     * you can skip columns that you do not want to show
     * like content, or description
     *
     * @return array
     */
    function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'id' => __('ID', 'maildata'),
            'tenfb' => __('Tên FB', 'maildata'),
            'uid' => __('UID FB', 'maildata'),
            'email' => __('EMAIL', 'maildata'),
            'created_at' => __('Ngày tạo', 'maildata'),
            'user_created' => __('Người tạo', 'maildata'),
        );
        return $columns;
    }

    /**
     * [OPTIONAL] This method return columns that may be used to sort table
     * all strings in array - is column names
     * notice that true on name column means that its default sort
     *
     * @return array
     */
    function get_sortable_columns()
    {
        $sortable_columns = array(

            'id' => array('id', true),
            'tenfb' => array('tenfb', false),
            'abc' => array('abc', false),
            'email' => array('email', false),
            'created_at' => array('created_at', false),
            'user_created' => array('user_created', false),
        );
        return $sortable_columns;
    }

    /**
     * [OPTIONAL] Return array of bult actions if has any
     *
     * @return array
     */
    function get_bulk_actions()
    {
        $actions = array(
            'delete' => 'Delete',
            'export_csv'=>'Tải xuống CSV'
        );
        return $actions;
    }

    /**
     * [OPTIONAL] This method processes bulk actions
     * it can be outside of class
     * it can not use wp_redirect coz there is output already
     * in this example we are processing delete action
     * message about successful deletion will be shown on page in next part
     */
    function process_bulk_action()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'etado_mail_data'; // do not forget about tables prefix
        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids)) $ids = implode(',', $ids);
            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        }
//        if('export_csv' == $this->current_action() )  {
//            echo 'hi';
//
//        }
    }

    /**
     * [REQUIRED] This is the most important method
     *
     * It will get rows from database and prepare them to be showed in table
     */
    function prepare_items()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'etado_mail_data'; // do not forget about tables prefix
        $per_page = 5; // constant, how much records will be shown per page
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);
        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();
        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");
        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged'] - 1) * $per_page) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';
        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $data_db = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);
        foreach ($data_db as $key => $value) :
            $data_db[$key]['user_created'] = get_the_author_meta( 'display_name' , $value['user_created'] );
            $data_db[$key]['created_at'] = date('d/m/Y', strtotime($value['created_at']));
        endforeach;
        $this->items = $data_db;
        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }
}

/**
 * PART 3. Admin page
 * ============================================================================
 *
 * In this part you are going to add admin page for custom table
 *
 * http://codex.wordpress.org/Administration_Menus
 */
/**
 * admin_menu hook implementation, will add pages to list persons and to add new one
 */
function maildata_admin_menu()
{
    add_menu_page(__('LIST EMAIL', 'maildata'), __('LIST EMAIL', 'maildata'), 'activate_plugins', 'list-email', 'maildata_persons_page_handler');
    add_submenu_page('list-email', __('LIST EMAIL', 'maildata'), __('LIST EMAIL', 'maildata'), 'activate_plugins', 'list-email', 'maildata_persons_page_handler');
    // add new will be described in next part
    add_submenu_page('list-email', __('Add new', 'maildata'), __('Add new', 'maildata'), 'activate_plugins', 'list_mail_form', 'maildata_list_mail_form_page_handler');
}

add_action('admin_menu', 'maildata_admin_menu');
/**
 * List page handler
 *
 * This function renders our custom table
 * Notice how we display message about successfull deletion
 * Actualy this is very easy, and you can add as many features
 * as you want.
 *
 * Look into /wp-admin/includes/class-wp-*-list-table.php for examples
 */
function maildata_persons_page_handler()
{
    global $wpdb;
    $table = new Data_Email_Table_Example_List_Table();
    $table->prepare_items();
    $message = '';
    if ('delete' === $table->current_action()) {
        $count = is_array($_REQUEST['id']) ? count($_REQUEST['id']) : $_REQUEST['id'];
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'maildata'), $count) . '</p></div>';
    }
    ?>
    <div class="wrap">

        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('LIST EMAIL', 'maildata') ?> <a class="add-new-h2"
                                                     href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=list_mail_form'); ?>"><?php _e('Add new', 'maildata') ?></a>
        </h2>
        <?php echo $message; ?>

        <form id="persons-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>

    </div>
    <?php
}

/**
 * PART 4. Form for adding andor editing row
 * ============================================================================
 *
 * In this part you are going to add admin page for adding andor editing items
 * You cant put all form into this function, but in this example form will
 * be placed into meta box, and if you want you can split your form into
 * as many meta boxes as you want
 *
 * http://codex.wordpress.org/Data_Validation
 * http://codex.wordpress.org/Function_Reference/selected
 */
/**
 * Form page handler checks is there some data posted and tries to save it
 * Also it renders basic wrapper in which we are callin meta box render
 */
function maildata_list_mail_form_page_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'etado_mail_data'; // do not forget about tables prefix
    $message = '';
    $notice = '';
    // this is default $item which will be used for new records
    $default = array(
        'id' => '',
        'tenfb' => '',
        'uid' => '',
        'email' => '',
        'user_created' => get_current_user_id()
    );
    // here we are verifying does this request is post back and have correct nonce
    if (isset($_REQUEST['nonce']) && wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {
        // combine our default item with request params
        $item = shortcode_atts($default, $_REQUEST);

        // validate data, and if all ok save item to database
        // if id is zero insert otherwise update
        $item_valid = maildata_validate_person($item);
        if ($item_valid === true) {
            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result) {
                    $message = __('Item was successfully saved', 'maildata');
                } else {
                    $notice = __('There was an error while saving item', 'maildata');
                }
            } else {
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                if ($result) {
                    $message = __('Item was successfully updated', 'maildata');
                } else {
                    $notice = __('There was an error while updating item', 'maildata');
                }
            }
        } else {
            // if $item_valid not true it contains error message(s)
            $notice = $item_valid;
        }
    } else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notice = __('Item not found', 'maildata');
            }
        }
    }
    // here we adding our custom meta box
    add_meta_box('list_mail_form_meta_box', 'Thông Tin', 'maildata_list_mail_form_meta_box_handler', 'meta_box_email', 'normal', 'default');
    ?>
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
        <h2><?php _e('Thông tin', 'maildata') ?> <a class="add-new-h2"
                                                 href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=persons'); ?>"><?php _e('back to list', 'maildata') ?></a>
        </h2>

        <?php if (!empty($notice)): ?>
            <div id="notice" class="error"><p><?php echo $notice ?></p></div>
        <?php endif; ?>
        <?php if (!empty($message)): ?>
            <div id="message" class="updated"><p><?php echo $message ?></p></div>
        <?php endif; ?>

        <form id="form" method="POST">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__)) ?>"/>
            <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
            <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

            <div class="metabox-holder" id="poststuff">
                <div id="post-body">
                    <div id="post-body-content">
                        <?php /* And here we call our custom meta box */ ?>
                        <?php do_meta_boxes('meta_box_email', 'normal', $item); ?>
                        <input type="submit" value="<?php _e('Save', 'maildata') ?>" id="submit"
                               class="button-primary" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php
}

/**
 * This function renders our custom meta box
 * $item is row
 *
 * @param $item
 */
function maildata_list_mail_form_meta_box_handler($item)
{
    ?>

    <table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
        <tbody>
        <tr class="form-field">
            <th valign="top" scope="row">
                <label for="name"><?php _e('Ten Facebook', 'maildata') ?></label>
            </th>
            <td>
                <input id="name" name="tenfb" type="text" style="width: 95%"
                       value="<?php echo esc_attr($item['tenfb']) ?>"
                       size="50" class="code" placeholder="<?php _e('Your name', 'maildata') ?>" required>
            </td>
        </tr>
        <tr class="form-field">
            <th valign="top" scope="row">
                <label for="name"><?php _e('UID FB', 'maildata') ?></label>
            </th>
            <td>
                <input id="name" name="uid" type="text" style="width: 95%" value="<?php echo esc_attr($item['uid']) ?>"
                       size="50" class="code" placeholder="<?php _e('Your name', 'maildata') ?>" required>
            </td>
        </tr>
        <tr class="form-field">
            <th valign="top" scope="row">
                <label for="name"><?php _e('EMAIL', 'maildata') ?></label>
            </th>
            <td>
                <input id="name" name="email" type="text" style="width: 95%"
                       value="<?php echo esc_attr($item['email']) ?>"
                       size="50" class="code" placeholder="<?php _e('Your name', 'maildata') ?>" required>
            </td>
        </tr>

        </tbody>
    </table>
    <?php
}

/**
 * Simple function that validates data and retrieve bool on success
 * and error message(s) on error
 *
 * @param $item
 * @return bool|string
 */
function maildata_validate_person($item)
{

    $messages = array();
    // if (empty($item['name'])) $messages[] = __('Name is required', 'maildata');

    if (empty($messages)) return true;
    return implode('<br />', $messages);
}

/**
 * Do not forget about translating your plugin, use __('english string', 'your_uniq_plugin_name') to retrieve translated string
 * and _e('english string', 'your_uniq_plugin_name') to echo it
 * in this example plugin your_uniq_plugin_name == maildata
 *
 * to create translation file, use poedit FileNew catalog...
 * Fill name of project, add "." to path (ENSURE that it was added - must be in list)
 * and on last tab add "__" and "_e"
 *
 * Name your file like this: [my_plugin]-[ru_RU].po
 *
 * http://codex.wordpress.org/Writing_a_Plugin#Internationalizing_Your_Plugin
 * http://codex.wordpress.org/I18n_for_WordPress_Developers
 */
function maildata_languages()
{
    load_plugin_textdomain('maildata', false, dirname(plugin_basename(__FILE__)));
}

add_action('init', 'maildata_languages');