<?php


// Register REST API endpoints
class GenerateWP_Custom_REST_API_Endpoints
{

    /**
     * Register the routes for the objects of the controller.
     */
    public static function register_endpoints()
    {
        // endpoints will be registered here
        register_rest_route('etadomail/v1', '/get', array(
            'methods' => WP_REST_Server::READABLE,
            'callback' => array('GenerateWP_Custom_REST_API_Endpoints', 'get_info'),
        ));
        register_rest_route('etadomail/v1', '/post', array(
            'methods' => WP_REST_Server::CREATABLE,
            'callback' => array('GenerateWP_Custom_REST_API_Endpoints', 'create_email'),
            'permission_callback' => array('GenerateWP_Custom_REST_API_Endpoints', 'create_email_permission')
        ));
    }

    public static function create_email_permission($request)
    {
        if (!is_user_logged_in()) {
            return new WP_Error('rest_forbidden', esc_html__('You do not have permissions to create data.', 'maildata'), array('status' => 401));
        }
        return true;
    }

    public static function create_email($request)
    {
    $post = $request->get_param('postdata');
    var_dump($post);
    die();
        global $wpdb;
        $table_name = $wpdb->prefix . 'etado_mail_data';
        $get_current_user_id = get_current_user_id();
        $item = array(
            array('tenfb' => '321312', 'uid' => '21321', 'email' => 'devqanh321312@gmail.com'),
            array('tenfb' => '321312', 'uid' => '21321', 'email' => 'devqanh3213@gmail.com'),
            array('tenfb' => '321312', 'uid' => '21321', 'email' => 'devqanh123@gmail.com'),
            array('tenfb' => '321312', 'uid' => '21321', 'email' => 'devqanh121312323@gmail.com')
        );

        foreach ($item as $value) :
            $kq = array_merge($value, array('user_created' => $get_current_user_id));
           // $result = $wpdb->insert($table_name, $kq);
        endforeach;
        return new WP_REST_Response($kq, 200);
    }

    public static function get_info()
    {
        return new WP_REST_Response(array('hi'), 200);
    }


}

add_action('rest_api_init', array('GenerateWP_Custom_REST_API_Endpoints', 'register_endpoints'));