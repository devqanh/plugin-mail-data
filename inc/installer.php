<?php

add_action('plugins_loaded', function (){
    global $etado_db_version;
    if (get_site_option('etado_db_version') != $etado_db_version) {
        etado_mail_data_install();
    }
});

function etado_mail_data_install()
{
    global $wpdb;
    global $etado_db_version;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'etado_mail_data'; // do not forget about tables prefix

    $sql = "CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		tenfb TEXT NOT NULL,
		uid INTEGER(20) NOT NULL,
		email TEXT NOT NULL,
		user_created INTEGER(20) NOT NULL,
		created_at TIMESTAMP NOT NULL DEFAULT NOW(),
		UNIQUE KEY id (id)
	) $charset_collate";

    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
    // save current database version for later use (on upgrade)
    add_option('etado_db_version', $etado_db_version);

    $installed_ver = get_option('etado_db_version');
    if ($installed_ver != $etado_db_version) {
        $sql ="CREATE TABLE $table_name (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		tenfb TEXT NOT NULL,
		uid INTEGER(20) NOT NULL,
		email TEXT NOT NULL,
		user_created INTEGER(20) NOT NULL,
		created_at TIMESTAMP NOT NULL DEFAULT NOW(),
		UNIQUE KEY id (id)
	) $charset_collate";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
        // notice that we are updating option, rather than adding it
        update_option('etado_db_version', $etado_db_version);
    }
}

register_activation_hook(__FILE__, 'etado_mail_data_install');


function etado_mail_install_data()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'cte'; // do not forget about tables prefix
    $wpdb->insert($table_name, array(
        'id' => '',
        'tenfb' => 'Quyenanh',
        'uid' => 25,
        'email'=>'devqanh@gmail.com',
        'created_at'=> '2019-09-26 00:00:00'
    ));
    $wpdb->insert($table_name, array(
        'id' => '',
        'tenfb' => 'Quyenanh 1',
        'uid' => 25,
        'email'=>'devqanh@gmail.com',
        'created_at'=> '2019-09-26 00:00:00'
    ));
    $wpdb->insert($table_name, array(
        'id' => '',
        'tenfb' => 'Quyenanh 2',
        'uid' => 25,
        'email'=>'devqanh@gmail.com',
        'created_at'=> '2019-09-26 00:00:00'
    ));
    $wpdb->insert($table_name, array(
        'id' => '',
        'tenfb' => 'Quyenanh 3',
        'uid' => 25,
        'email'=>'devqanh@gmail.com',
        'created_at'=> '2019-09-26 00:00:00'
    ));
    $wpdb->insert($table_name, array(
        'id' => '',
        'tenfb' => 'Quyenanh 4',
        'uid' => 25,
        'email'=>'devqanh@gmail.com',
        'created_at'=> '2019-09-26 00:00:00'
    ));

}
register_activation_hook(__FILE__, 'etado_mail_install_data');