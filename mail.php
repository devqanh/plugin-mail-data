<?php
/*
Plugin Name: Mail data
Plugin URI: https://akismet.com/
Description: Used by millions, Akismet is quite possibly the best way in the world to <strong>protect your blog from spam</strong>. It keeps your site protected even while you sleep. To get started: activate the Akismet plugin and then go to your Akismet Settings page to set up your API key.
Version: 2.3
Author: Automattic
Author URI: https://automattic.com/wordpress-plugins/
License: GPLv2 or later
Text Domain: maildata
*/
global $etado_db_version;
$etado_db_version = '2.3';
require_once('inc/installer.php');
require_once('inc/show_table.php');



